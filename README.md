# cxf-rt-javassist
通过javassist生成基于cxf的jaxws、jaxrs实现

### Maven Dependency

``` xml
<dependency>
	<groupId>com.github.vindell</groupId>
	<artifactId>cxf-rt-javassist</artifactId>
	<version>${project.version}</version>
</dependency>
```
